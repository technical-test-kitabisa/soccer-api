package main

import (
	"github.com/annafia/soccer/config"
	"github.com/annafia/soccer/database"
	"github.com/annafia/soccer/routes"
)

func main() {
	conf := config.GetConfig()

	database.Init()

	app := routes.Init()

	app.Logger.Fatal(app.Start(conf.APP_HOST + ":" + conf.APP_PORT))
}
