# Technical Test by Kitabisa.com | soccer-api

[![Build Status](https://travis-ci.com/iswanulumam/blog-tdd.svg?token=phg4cxxqtEs42hsRYxcv&branch=master)](https://travis-ci.com/iswanulumam/blog-tdd)

Run: `go run main.go` || Run with Watcher : `watcher`

## Stack-tech :dart:
- [x] RESTful API Using Go, Echo, Gorm, Postgres

## Open Endpoints
Open endpoints require no Authentication.

### Nationality related

Each endpoint manipulates or displays information related to the Nationality of players is provided with this request:

* Show all nationality : `GET /nationality`
    * Without params
* Create new nationality : `POST /nationality`
    * NationalityName (String)
* Update/Modify nationality : `PUT /nationality`
    * NationalityID (Int64)
    * NationalityName (String)
* Delete nationality : `DELETE /nationality`
    * NationalityID (Int64)

### Club related

Each endpoint manipulates or displays information related to the Soccer Club is provided with this request:

* Show all club : `GET /club`
    * Without params
* Create new club : `POST /club`
    * ClubName (String)
    * ClubInitial (String)
* Update/Modify club : `PUT /club`
    * ClubCode (String)
    * ClubName (String)
    * ClubInitial (String)
* Delete club : `DELETE /club`
    * ClubCode (String)

### Player related

Each endpoint manipulates or displays information related to the Soccer Players is provided with this request:

* Show all player : `GET /player`
    * Without params
* Create new player : `POST /player`
    * PlayerName (String)
    * PlayerNationalityID (Int64)
    * PlayerDob (String of Date): "dd/mm/yyyy"
* Update/Modify player : `PUT /player`
    * PlayerCode (String)
    * PlayerName (String)
    * PlayerNationalityID (Int64)
    * PlayerDob (String of Date): "dd/mm/yyyy"
* Delete player : `DELETE /player`
    * PlayerCode (String)


### Player Club related

Each endpoint manipulates or displays information related to the Member of Soccer Clubs are provided with this request:

* Show detail club with all members : `GET /players-club`
    * ClubCode (String)
* Assign player to a club : `POST /players-club`
    * ClubCode (String)
    * PlayerCode (String)
    * AssignedDate (String of Date): "dd/mm/yyyy"
* Unassign player from a club : `DELETE /players-club`
    * PlayersClubID (Int64)