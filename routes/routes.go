package routes

import (
	"github.com/annafia/soccer/controllers/club"
	"github.com/annafia/soccer/controllers/nationality"
	"github.com/annafia/soccer/controllers/player"
	playersclub "github.com/annafia/soccer/controllers/players-club"
	"github.com/go-playground/validator"

	"github.com/labstack/echo"
)

type (
	CustomValidator struct {
		validator *validator.Validate
	}
)

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func Init() *echo.Echo {
	router := echo.New()
	router.Validator = &CustomValidator{validator: validator.New()}

	router.GET("/nationality", nationality.Get)
	router.POST("/nationality", nationality.Create)
	router.PUT("/nationality", nationality.Update)
	router.DELETE("/nationality", nationality.Delete)

	router.GET("/club", club.Get)
	router.POST("/club", club.Create)
	router.PUT("/club", club.Update)
	router.DELETE("/club", club.Delete)

	router.GET("/player", player.Get)
	router.POST("/player", player.Create)
	router.PUT("/player", player.Update)
	router.DELETE("/player", player.Delete)

	router.GET("/players-club", playersclub.Get)
	router.POST("/players-club", playersclub.Create)
	router.DELETE("/players-club", playersclub.Remove)

	return router
}
