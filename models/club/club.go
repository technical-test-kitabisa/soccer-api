package club

import (
	"time"

	"github.com/annafia/soccer/database"
	"github.com/annafia/soccer/database/schema"
	uuid "github.com/nu7hatch/gouuid"
	"gorm.io/gorm"
)

func FindByCode(ClubCode string) (schema.Club, error) {
	var DB = database.DBConn

	var club schema.Club

	DB.Model(&schema.Club{}).Where("code=? AND statusid=?", ClubCode, 1).First(&club)

	return club, nil
}

func FetchAll() ([]schema.Club, error) {
	var DB = database.DBConn

	var clubs []schema.Club
	DB.Model(&schema.Club{}).Where("statusid=?", 1).Find(&clubs)

	return clubs, nil
}

func Create(ClubName string, ClubInitial string) (schema.Club, error) {
	var DB = database.DBConn
	var club schema.Club

	ClubCode, err := uuid.NewV4()

	if err != nil {
		return club, err
	}

	club.Code = ClubCode.String()
	club.Name = ClubName
	club.Initial = ClubInitial
	club.CreatedAt = time.Now()
	club.Statusid = 1

	create := DB.Select("Code", "Name", "Initial", "CreatedAt", "Statusid").Create(&club)

	if create.Error != nil {
		return club, create.Error
	}

	return club, nil
}

func Update(ClubCode string, ClubName string, ClubInitial string) (schema.Club, error) {
	var DB = database.DBConn
	var club schema.Club

	err := DB.Transaction(func(tx *gorm.DB) error {
		DB.Model(&schema.Club{}).Where("code=? AND statusid=?", ClubCode, 1).First(&club)

		update := DB.Model(&club).Update("statusid", "0")
		if update.Error != nil {
			return update.Error
		}

		club.Name = ClubName
		club.Initial = ClubInitial
		club.CreatedAt = time.Now()
		club.Statusid = 1
		create := DB.Select("Code", "Name", "Initial", "CreatedAt", "Statusid").Create(&club)
		if create.Error != nil {
			return create.Error
		}

		return nil
	})

	if err != nil {
		return club, err
	}

	return club, nil
}

func Delete(ClubCode string) (schema.Club, error) {
	var DB = database.DBConn
	var club schema.Club

	err := DB.Transaction(func(tx *gorm.DB) error {
		DB.Model(&schema.Club{}).Where("code=? AND statusid=?", ClubCode, 1).First(&club)

		update := DB.Model(&club).Update("statusid", "0")
		if update.Error != nil {
			return update.Error
		}

		club.CreatedAt = time.Now()
		create := DB.Select("Code", "Name", "Initial", "CreatedAt", "Statusid").Create(&club)
		if create.Error != nil {
			return create.Error
		}

		return nil
	})

	if err != nil {
		return club, err
	}

	return club, nil
}
