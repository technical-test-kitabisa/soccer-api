package player

import (
	"time"

	"github.com/annafia/soccer/database"
	"github.com/annafia/soccer/database/schema"
	uuid "github.com/nu7hatch/gouuid"
	"gorm.io/gorm"
)

func FindByCode(PlayerCode string) (schema.Player, error) {
	var DB = database.DBConn

	var player schema.Player

	DB.Model(&schema.Player{}).Where("code=? AND statusid=?", PlayerCode, 1).First(&player)

	return player, nil
}

func FetchAll() ([]schema.Player, error) {
	var DB = database.DBConn

	var players []schema.Player
	DB.Model(&schema.Player{}).Where("statusid=?", 1).Find(&players)

	return players, nil
}

func Create(PlayerName string, PlayerNationalityID int64, PlayerDob time.Time) (schema.Player, error) {
	var DB = database.DBConn
	var player schema.Player

	PlayerCode, err := uuid.NewV4()

	if err != nil {
		return player, err
	}

	player.Code = PlayerCode.String()
	player.Name = PlayerName
	player.NationalityID = PlayerNationalityID
	player.Dob = PlayerDob
	player.Active = 1
	player.CreatedAt = time.Now()
	player.Statusid = 1

	create := DB.Select("Code", "Name", "NationalityID", "Dob", "Active", "CreatedAt", "Statusid").Create(&player)

	if create.Error != nil {
		return player, create.Error
	}

	return player, nil
}

func Update(PlayerCode string, PlayerName string, PlayerNationalityID int64, PlayerDob time.Time) (schema.Player, error) {
	var DB = database.DBConn
	var player schema.Player

	err := DB.Transaction(func(tx *gorm.DB) error {
		DB.Model(&schema.Player{}).Where("code=? AND statusid=?", PlayerCode, 1).First(&player)

		update := DB.Model(&player).Update("statusid", "0")
		if update.Error != nil {
			return update.Error
		}

		player.Name = PlayerName
		player.NationalityID = PlayerNationalityID
		player.Dob = PlayerDob
		player.Active = 1
		player.CreatedAt = time.Now()
		player.Statusid = 1
		create := DB.Select("Code", "Name", "NationalityID", "Dob", "Active", "CreatedAt", "Statusid").Create(&player)
		if create.Error != nil {
			return create.Error
		}

		return nil
	})

	if err != nil {
		return player, err
	}

	return player, nil
}

func Delete(PlayerCode string) (schema.Player, error) {
	var DB = database.DBConn
	var player schema.Player

	err := DB.Transaction(func(tx *gorm.DB) error {
		DB.Model(&schema.Player{}).Where("code=? AND statusid=?", PlayerCode, 1).First(&player)

		update := DB.Model(&player).Update("statusid", "0")
		if update.Error != nil {
			return update.Error
		}

		player.CreatedAt = time.Now()
		player.Statusid = 0
		create := DB.Select("Code", "Name", "NationalityID", "Dob", "Active", "CreatedAt", "Statusid").Create(&player)
		if create.Error != nil {
			return create.Error
		}

		return nil
	})

	if err != nil {
		return player, err
	}

	return player, nil
}
