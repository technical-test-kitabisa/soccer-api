package playersclub

import (
	"time"

	"github.com/annafia/soccer/database"
	"github.com/annafia/soccer/database/schema"
	"gorm.io/gorm"
)

func FindByID(PlayersClubID int64) (schema.PlayersClub, error) {
	var DB = database.DBConn

	var playerclub schema.PlayersClub

	DB.Model(&schema.PlayersClub{}).Where("id=? AND statusid=?", PlayersClubID, 1).First(&playerclub)

	return playerclub, nil
}

func FindPlayersClub(PlayersID string, ClubID string) (schema.PlayersClub, error) {
	var DB = database.DBConn

	var playerclub schema.PlayersClub

	DB.Model(&schema.PlayersClub{}).Where("players_id=? AND club_id=? AND statusid=?", PlayersID, ClubID, 1).First(&playerclub)

	return playerclub, nil
}

func FetchAll(ClubID string) ([]schema.PlayersClub, error) {
	var DB = database.DBConn

	var players []schema.PlayersClub
	DB.Model(&schema.PlayersClub{}).Where("club_id=? AND statusid=?", ClubID, 1).Find(&players)

	return players, nil
}

func Create(PlayerID string, ClubID string, AssignedDate time.Time) (schema.PlayersClub, error) {
	var DB = database.DBConn
	var playerclub schema.PlayersClub

	playerclub.PlayersID = PlayerID
	playerclub.ClubID = ClubID
	playerclub.AssignedDate = AssignedDate
	playerclub.CreatedAt = time.Now()
	playerclub.Statusid = 1

	create := DB.Select("PlayersID", "ClubID", "AssignedDate", "CreatedAt", "Statusid").Create(&playerclub)

	if create.Error != nil {
		return playerclub, create.Error
	}

	return playerclub, nil
}

func Remove(PlayersClubID int64) (schema.PlayersClub, error) {
	var DB = database.DBConn
	var playerclub schema.PlayersClub

	err := DB.Transaction(func(tx *gorm.DB) error {
		DB.Model(&schema.PlayersClub{}).Where("id=? AND statusid=?", PlayersClubID, 1).First(&playerclub)

		update := DB.Model(&playerclub).Update("statusid", "0")
		if update.Error != nil {
			return update.Error
		}

		playerclub.CreatedAt = time.Now()
		playerclub.Statusid = 0
		create := DB.Select("PlayersID", "ClubID", "AssignedDate", "CreatedAt", "Statusid").Create(&playerclub)
		if create.Error != nil {
			return create.Error
		}

		return nil
	})

	if err != nil {
		return playerclub, err
	}

	return playerclub, nil
}
