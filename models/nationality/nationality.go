package nationality

import (
	"github.com/annafia/soccer/database"
	"github.com/annafia/soccer/database/schema"
)

// For scanning rows
var nationality schema.Nationality

func FindByID(NationalityID int64) (schema.Nationality, error) {
	var DB = database.DBConn

	var nationality schema.Nationality

	DB.Model(&schema.Nationality{}).Where("id=? AND statusid=?", NationalityID, 1).First(&nationality)

	return nationality, nil
}

func FetchAll() ([]schema.Nationality, error) {
	var DB = database.DBConn

	var nationalities []schema.Nationality
	DB.Model(&schema.Nationality{}).Where("statusid=?", 1).Find(&nationalities)

	return nationalities, nil
}

func Create(NationalityName string) (schema.Nationality, error) {
	var DB = database.DBConn

	var nationality schema.Nationality

	nationality.Nationality = NationalityName
	nationality.Statusid = 1

	create := DB.Select("Nationality", "Statusid").Create(&nationality)

	if create.Error != nil {
		return nationality, create.Error
	}

	return nationality, nil
}

func Update(NationalityID int64, NationalityName string) (schema.Nationality, error) {
	var DB = database.DBConn

	var nationality schema.Nationality

	nationality.ID = NationalityID
	nationality.Statusid = 1

	update := DB.Model(&nationality).Updates(nationality)

	if update.Error != nil {
		return nationality, update.Error
	}

	return nationality, nil
}

func Delete(NationalityID int64) (schema.Nationality, error) {
	var DB = database.DBConn

	var nationality schema.Nationality
	nationality.ID = NationalityID

	update := DB.Model(&nationality).Select("Statusid").Update("Statusid", 0)

	if update.Error != nil {
		return nationality, update.Error
	}

	return nationality, nil
}
