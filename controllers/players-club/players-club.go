package playersclub

import (
	"net/http"
	"time"

	"github.com/annafia/soccer/database/schema"
	"github.com/annafia/soccer/models/club"
	"github.com/annafia/soccer/models/player"
	playersclub "github.com/annafia/soccer/models/players-club"
	"github.com/labstack/echo"
)

func Get(ctx echo.Context) error {
	var res schema.Response
	type PlayersClub struct {
		ClubID string `json:"clubID" validate:"required"`
	}

	body := new(PlayersClub)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	findClub, findClubErr := club.FindByCode(body.ClubID)
	if findClubErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findClubErr.Error()})
	}

	if findClub.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Club not found"})
	}

	club, clubErr := club.FindByCode(body.ClubID)
	if clubErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": clubErr.Error()})
	}

	playersclubs, err := playersclub.FetchAll(body.ClubID)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	DataMaps := make(map[string]interface{})
	DataMaps["club"] = club
	DataMaps["palyers"] = playersclubs

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = DataMaps

	return ctx.JSON(http.StatusOK, res)
}

func Create(ctx echo.Context) error {
	var res schema.Response
	type PlayersClub struct {
		PlayerID     string `json:"playerID" validate:"required"`
		ClubID       string `json:"clubID" validate:"required"`
		AssignedDate string `json:"assignedDate" validate:"required"`
	}

	body := new(PlayersClub)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	DateFormat := "02/01/2006"
	AssignedDate, AssignedDateError := time.Parse(DateFormat, body.AssignedDate)
	if AssignedDateError != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Invalid date format"})
	}

	findPlayer, findPlayerErr := player.FindByCode(body.PlayerID)
	if findPlayerErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findPlayerErr.Error()})
	}

	if findPlayer.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Player not found"})
	}

	findClub, findClubErr := club.FindByCode(body.ClubID)
	if findClubErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findClubErr.Error()})
	}

	if findClub.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Club not found"})
	}

	findPlayersClub, findPlayersClubErr := playersclub.FindPlayersClub(body.PlayerID, body.ClubID)
	if findPlayersClubErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findPlayersClubErr.Error()})
	}

	if findPlayersClub.ID != 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Player is already assigned"})
	}

	create, err := playersclub.Create(body.PlayerID, body.ClubID, AssignedDate)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = create

	return ctx.JSON(http.StatusOK, res)
}

func Remove(ctx echo.Context) error {
	var res schema.Response
	type PlayersClub struct {
		PlayersClubID int64 `json:"playersClubID" validate:"required,numeric"`
	}

	body := new(PlayersClub)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	findPlayersClub, findPlayersClubErr := playersclub.FindByID(body.PlayersClubID)
	if findPlayersClubErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findPlayersClubErr.Error()})
	}

	if findPlayersClub.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Member not found"})
	}

	remove, err := playersclub.Remove(body.PlayersClubID)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = remove

	return ctx.JSON(http.StatusOK, res)
}
