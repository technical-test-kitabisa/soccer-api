package player

import (
	"net/http"
	"time"

	"github.com/annafia/soccer/database/schema"
	"github.com/annafia/soccer/models/nationality"
	"github.com/annafia/soccer/models/player"
	"github.com/labstack/echo"
)

func Get(ctx echo.Context) error {
	var res schema.Response
	player, err := player.FetchAll()

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = player

	return ctx.JSON(http.StatusOK, res)
}

func Create(ctx echo.Context) error {
	var res schema.Response
	type Player struct {
		PlayerName          string `json:"playerName" validate:"required"`
		PlayerNationalityID int64  `json:"playerNationalityID" validate:"required,numeric"`
		PlayerDob           string `json:"playerDob" validate:"required"`
	}

	body := new(Player)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	DateFormat := "02/01/2006"
	Dob, DobError := time.Parse(DateFormat, body.PlayerDob)
	if DobError != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Invalid date format"})
	}

	findNationality, findNationalityErr := nationality.FindByID(body.PlayerNationalityID)
	if findNationalityErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findNationalityErr.Error()})
	}

	if findNationality.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	create, err := player.Create(body.PlayerName, body.PlayerNationalityID, Dob)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = create

	return ctx.JSON(http.StatusOK, res)
}

func Update(ctx echo.Context) error {
	var res schema.Response
	type Player struct {
		PlayerCode          string `json:"playerCode" validate:"required"`
		PlayerName          string `json:"playerName" validate:"required"`
		PlayerNationalityID int64  `json:"playerNationalityID" validate:"required,numeric"`
		PlayerDob           string `json:"playerDob" validate:"required"`
	}

	body := new(Player)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	DateFormat := "02/01/2006"
	Dob, DobError := time.Parse(DateFormat, body.PlayerDob)
	if DobError != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Invalid date format"})
	}

	findPlayer, findPlayerErr := player.FindByCode(body.PlayerCode)
	if findPlayerErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findPlayerErr.Error()})
	}

	if findPlayer.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	findNationality, findNationalityErr := nationality.FindByID(body.PlayerNationalityID)
	if findNationalityErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findNationalityErr.Error()})
	}

	if findNationality.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	update, err := player.Update(body.PlayerCode, body.PlayerName, body.PlayerNationalityID, Dob)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = update

	return ctx.JSON(http.StatusOK, res)
}

func Delete(ctx echo.Context) error {
	var res schema.Response
	type Player struct {
		PlayerCode string `json:"playerCode" validate:"required"`
	}

	body := new(Player)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	findPlayer, findPlayerErr := player.FindByCode(body.PlayerCode)
	if findPlayerErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findPlayerErr.Error()})
	}

	if findPlayer.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	delete, err := player.Delete(body.PlayerCode)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = delete

	return ctx.JSON(http.StatusOK, res)
}
