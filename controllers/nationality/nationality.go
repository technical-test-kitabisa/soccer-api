package nationality

import (
	"net/http"

	"github.com/annafia/soccer/database/schema"
	"github.com/annafia/soccer/models/nationality"
	"github.com/labstack/echo"
)

func Get(ctx echo.Context) error {
	var res schema.Response
	nationality, err := nationality.FetchAll()

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = nationality

	return ctx.JSON(http.StatusOK, res)
}

func Create(ctx echo.Context) error {
	var res schema.Response
	type Nationality struct {
		NationalityName string `json:"nationalityName" validate:"required"`
	}

	body := new(Nationality)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	create, err := nationality.Create(body.NationalityName)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = create

	return ctx.JSON(http.StatusOK, res)
}

func Update(ctx echo.Context) error {
	var res schema.Response

	type Nationality struct {
		NationalityID   int64  `json:"nationalityid" validate:"required,numeric"`
		NationalityName string `json:"nationalityName" validate:"required"`
	}

	body := new(Nationality)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	find, findErr := nationality.FindByID(body.NationalityID)
	if findErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findErr.Error()})
	}

	if find.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	update, err := nationality.Update(body.NationalityID, body.NationalityName)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = update

	return ctx.JSON(http.StatusOK, res)
}

func Delete(ctx echo.Context) error {
	var res schema.Response

	type Nationality struct {
		NationalityID int64 `json:"nationalityid" validate:"required,numeric"`
	}

	body := new(Nationality)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	find, findErr := nationality.FindByID(body.NationalityID)
	if findErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findErr.Error()})
	}

	if find.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	delete, err := nationality.Delete(body.NationalityID)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	delete.Nationality = find.Nationality

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = delete

	return ctx.JSON(http.StatusOK, res)
}
