package club

import (
	"net/http"

	"github.com/annafia/soccer/database/schema"
	"github.com/annafia/soccer/models/club"
	"github.com/labstack/echo"
)

func Get(ctx echo.Context) error {
	var res schema.Response
	club, err := club.FetchAll()

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = club

	return ctx.JSON(http.StatusOK, res)
}

func Create(ctx echo.Context) error {
	var res schema.Response
	type Club struct {
		ClubName    string `json:"clubName" validate:"required"`
		ClubInitial string `json:"clubInitial" validate:"required"`
	}

	body := new(Club)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	create, err := club.Create(body.ClubName, body.ClubInitial)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = create

	return ctx.JSON(http.StatusOK, res)
}

func Update(ctx echo.Context) error {
	var res schema.Response
	type Club struct {
		ClubCode    string `json:"clubCode" validate:"required"`
		ClubName    string `json:"clubName" validate:"required"`
		ClubInitial string `json:"clubInitial" validate:"required"`
	}

	body := new(Club)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	find, findErr := club.FindByCode(body.ClubCode)
	if findErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findErr.Error()})
	}

	if find.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	update, err := club.Update(body.ClubCode, body.ClubName, body.ClubInitial)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = update

	return ctx.JSON(http.StatusOK, res)
}

func Delete(ctx echo.Context) error {
	var res schema.Response
	type Club struct {
		ClubCode string `json:"clubCode" validate:"required"`
	}

	body := new(Club)
	bind, validate := ctx.Bind(body), ctx.Validate(body)
	if bind != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": bind.Error()})
	} else if validate != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": validate.Error()})
	}

	find, findErr := club.FindByCode(body.ClubCode)
	if findErr != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": findErr.Error()})
	}

	if find.ID == 0 {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": "Data not found"})
	}

	delete, err := club.Delete(body.ClubCode)

	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = delete

	return ctx.JSON(http.StatusOK, res)
}
