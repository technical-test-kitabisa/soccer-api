package config

import "github.com/tkanos/gonfig"

type Configuration struct {
	APP_NAME    string
	APP_HOST    string
	APP_PORT    string
	DB_HOST     string
	DB_USERNAME string
	DB_PASSWORD string
	DB_PORT     string
	DB_NAME     string
}

func GetConfig() Configuration {
	conf := Configuration{}
	gonfig.GetConf("config/config.json", &conf)
	return conf
}
