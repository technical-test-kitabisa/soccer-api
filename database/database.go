package database

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"github.com/annafia/soccer/config"
	"github.com/annafia/soccer/database/schema"
)

var DBConn *gorm.DB
var err error

// Init Database Connection
func Init() {
	conf := config.GetConfig()

	dbInfo := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable TimeZone=Asia/Shanghai",
		conf.DB_HOST, conf.DB_PORT, conf.DB_USERNAME, conf.DB_PASSWORD, conf.DB_NAME,
	)

	DBConn, err = gorm.Open(postgres.Open(dbInfo), &gorm.Config{})

	DBConn.AutoMigrate(
		&schema.Nationality{},
		&schema.Player{},
		&schema.Club{},
		&schema.Player{},
		&schema.PlayersClub{})

	if err != nil {
		panic(err)
	}
}
