package schema

import "time"

type Club struct {
	ID        int64  `gorm:"primaryKey;autoIncrement"`
	Code      string `gorm:"index"`
	Name      string
	Initial   string    `gorm:"index"`
	CreatedAt time.Time `gorm:"index"`
	Statusid  int16     `gorm:"index"`
}
