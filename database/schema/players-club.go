package schema

import "time"

type PlayersClub struct {
	ID           int64     `gorm:"primaryKey;autoIncrement"`
	PlayersID    string    `gorm:"index"`
	ClubID       string    `gorm:"index"`
	AssignedDate time.Time `gorm:"index"`
	CreatedAt    time.Time `gorm:"index"`
	Statusid     int16     `gorm:"index"`
}
