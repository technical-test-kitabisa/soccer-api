package schema

type Nationality struct {
	ID          int64 `gorm:"primaryKey;autoIncrement"`
	Nationality string
	Statusid    int16 `gorm:"index"`
}
