package schema

import "time"

type Player struct {
	ID            int64 `gorm:"primaryKey;autoIncrement"`
	Code          string
	Name          string `gorm:"index"`
	NationalityID int64
	Dob           time.Time
	Active        int16 `gorm:"index"`
	CreatedAt     time.Time
	Statusid      int16 `gorm:"index"`
}
